﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Calance_API_Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Run().Wait();
        }

        static async Task Run()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://bitbucket.org/calanceus/api-test");
            string commits = client.GetStringAsync("https://api.bitbucket.org/2.0/repositories/calanceus/api-test/commits").Result;
            List<string> messages = new List<string>();
            for(int i = commits.LastIndexOf("\"message\":"); i > 0; i = commits.LastIndexOf("\"message\":"))
            {
                string message = commits.Substring(i+12);
                message = message.Substring(0, message.IndexOf("\", \"type\""));
                messages.Add(Regex.Unescape(message));
                commits = commits.Substring(0, i);
            }
            File.WriteAllLines("Commit Messages.txt", messages);
        }
    }
}
